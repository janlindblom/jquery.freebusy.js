(($) ->

  ###!
   * The main function. Inserts a free/busy widget at a given target element.
   * 
   * @param options set of options to pass to the function
   * @option options [Array] :urls list of urls to icalendar feeds
  ###
  $.fn.freebusy = (options) ->
    # merge settings hash with input options
    settings = $.extend({
      urls: []
    }, options)

    # Return "this" to support chaining
    this

  return
) (Zepto ? jQuery)
