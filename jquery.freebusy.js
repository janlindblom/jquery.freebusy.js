/*!
 * jquery.freebusy.js - A free/busy widget for jquery and zepto.
 * @version v0.0.1
 * @author Jan Lindblom <janlindblom@fastmail.fm>
 * @link https://bitbucket.org/janlindblom/jquery.freebusy.js
 * @license MIT
 * Copyright © 2015 Jan Lindblom <janlindblom@fastmail.fm>
 */
(function($) {

  /*!
   * The main function. Inserts a free/busy widget at a given target element.
   * 
   * @param options set of options to pass to the function
   * @option options [Array] :urls list of urls to icalendar feeds
   */
  $.fn.freebusy = function(options) {
    var settings;
    settings = $.extend({
      urls: []
    }, options);
    return this;
  };
})(typeof Zepto !== "undefined" && Zepto !== null ? Zepto : jQuery);
