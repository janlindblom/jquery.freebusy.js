# jquery.freebusy.js

A free/busy calendar widget as a jQuery/Zepto plugin.

## Usage

Use it like so:

```javascript
$("div#freebusy").freebusy(urls: ['https://user.fm/.../etc.ics']);
```
