module.exports = (grunt) ->
  # Project configuration.
  grunt.initConfig
    pkg: grunt.file.readJSON('package.json')
    banner: '/*!\n * <%= pkg.name %> - <%= pkg.description %>\n' +
            ' * @version v<%= pkg.version %>\n' +
            ' * @author <%= pkg.author %>\n' +
            ' * @link <%= pkg.homepage %>\n' +
            ' * @license <%= pkg.license %>\n' +
            ' * Copyright © <%= grunt.template.today("yyyy") %> <%= pkg.author %>\n */',
    coffee:
      library:
        options:
          bare: true
          join: true
        files:
          'jquery.freebusy.js': [
            'src/**/*.coffee'
          ]
    jshint:
      all:
        options:
          expr: true
          eqnull: true
        files: 
          src: [ 'jquery.freebusy.js' ]
    mochaTest:
      test:
        options:
          reporter: 'spec'
          quiet: false
          require: 'coffee-script/register'
        src: [ 'spec/**/*.coffee' ]
    uglify:
      all:
        options:
          banner: '<%= banner %>'
          sourceMap: true
          mangle:
            except: [
              '$'
              'jQuery'
              'Zepto'
            ]
        files:
          'jquery.freebusy.min.js': [
            'jquery.freebusy.js'
          ]
    usebanner:
      dist:
        options:
          position: 'top'
          banner: '<%= banner %>'
        files:
          src: ['jquery.freebusy.js']
    zip:
      dist:
        dest: 'pkg/<%= pkg.name %>-<%= pkg.version %>.zip'
        src: [
          'jquery.freebusy.js'
          'jquery.freebusy.min.js'
          'jquery.freebusy.min.js.map'
        ]

  grunt.loadNpmTasks 'grunt-contrib-coffee'
  grunt.loadNpmTasks 'grunt-contrib-jshint'
  grunt.loadNpmTasks 'grunt-contrib-uglify'
  grunt.loadNpmTasks 'grunt-banner'
  grunt.loadNpmTasks 'grunt-mocha-test'
  grunt.loadNpmTasks 'grunt-zip'

  grunt.registerTask 'build', [ 'coffee', 'usebanner', 'uglify' ]
  grunt.registerTask 'test', [ 'jshint', 'mochaTest' ]
  grunt.registerTask 'default', [ 'build', 'test' ]
  grunt.registerTask 'package', [ 'zip' ]
  return